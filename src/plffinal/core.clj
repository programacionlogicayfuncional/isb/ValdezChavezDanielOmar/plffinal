(ns plffinal.core)

(defn regresa-al-punto-origen? 
  [s]
  (letfn [(f [s acc x y]
            (if (zero? (count s)) ;;primera condicion, si no hay nada en la cadena 
              (if (== acc 0) 
                true
                (if (and (== x 0) (== y 0))
                  true
                  false))                
              (if (zero? (compare (subs s (dec (count s)) (count s)) ">")) ;;este
                (f (subs s 0 (dec (count s))) (inc acc) (inc x) y)

                (if (zero? (compare (subs s (dec (count s)) (count s)) "<")) ;;oeste
                  (f (subs s 0 (dec (count s))) (inc acc) (dec x) y)

                  (if (zero? (compare (subs s (dec (count s)) (count s)) "^")) ;;norte
                    (f (subs s 0 (dec (count s))) (inc acc) x (inc y))

                    (if (zero? (compare (subs s (dec (count s)) (count s)) "v")) ;;sur
                      (f (subs s 0 (dec (count s))) (inc acc) x (dec y))

                      (f (subs s 0 (dec (count s))) (inc acc) x y)
                      ))))))]
    (f s 0 0 0)))

(regresa-al-punto-origen? "")  
(regresa-al-punto-origen? "<}>>")  
(regresa-al-punto-origen? "<^v>")  
